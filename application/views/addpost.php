<?php include('header.php') ?>
	<title>Add Post</title>


</head>
<body>

<div id="container">
<h1>Add Post</h1>
<?php echo validation_errors(); ?>

<?php echo form_open('posts/add') ?>

	<label for="title">Title</label><br>
	<input type="input" name="title" /><br />

	<label for="text">Text</label><br>
	<textarea name="text" style='width : 100%; height: 250px'></textarea><br />
	<label for="categoriesid">Category</label><br>
	<select name='categoriesid'>
	<?php 
		foreach($cats->result() as $cat){
			echo "<option value='{$cat->categoriesid}'>{$cat->child}</option>";
		}
	?>
	</select><br><br>
	<input type='hidden' name='user' value='1' >
	<input type="submit" name="submit" value="Create Post" />

</form>
</div>

</body>
</html>