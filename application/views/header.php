<!DOCTYPE html>
<html lang="en">
<head>
	    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="js/bootstrap.min.js"></script>
	  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<meta charset="utf-8">	
	<style>
	.img-center {margin:0 auto;}

	</style>

</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url('.') ?>">List</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		<?php if (!isset($_COOKIE['usersid'])){ ?>
        <li><a href="<?php echo base_url('index.php/auth/index') ?>">Log In </a></li>
		<?php } ?>
		<li><a href="<?php echo base_url('index.php/auth/register') ?>">Register</a></li>
		<?php if (isset($_COOKIE['usersid'])){ ?>
		<li><a href="<?php echo base_url('index.php/auth/editform') ?>">Change Password</a></li>		
		<li><a href="<?php echo base_url('index.php/posts/addform') ?>">Add Posts</a></li>		
		<li><a href="<?php echo base_url('index.php/auth/logout') ?>">Log Out</a></li>		
		<?php } ?>
		<?php if (isset($_COOKIE['isadmin'])){ ?>
		<li><a href="<?php echo base_url('index.php/categories/index') ?>">Categories List</a></li>		
		<li><a href="<?php echo base_url('index.php/categories/addform') ?>">Add Categories</a></li>		
		<?php } ?>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container">