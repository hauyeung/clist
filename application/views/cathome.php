<?php include('header.php') ?>
<title>Categories</title>
</head>
<body>

<div id="container">
<table class="table">
	<tr>
		<td>Category</td>
		<td>Edit</td>
		<td>Delete</td>
	</tr>
<?php
	foreach ($cats->result() as $cat){
		$deleteurl = base_url("index.php/categories/delete/{$cat->categoriesid}");
		$editurl = base_url("index.php/categories/editform/{$cat->categoriesid}");
		echo '<tr>';
		echo "<td>{$cat->name}</td>";
		echo "<td><a href='{$editurl}'>Edit</a></td>";
		echo "<td><a href='{$deleteurl}'>Delete</a></td>";
		echo '</tr>';
	}
?>
</table>
</div>

</body>
</html>