<?php include('header.php') ?>
	<title>Add Category</title>


</head>
<body>

<div id="container">
<h1>Add Category</h1>
<?php echo validation_errors(); ?>

<?php echo form_open('categories/add') ?>

	<label for="title">Name</label><br>
	<input type="input" name="name" /><br />

	<label for="text">Child of: </label><br>
	<select name='parentcategoriesid'><br>
	<?php
		foreach($parentcats->result() as $cat){
			echo "<option value='{$cat->categoriesid}'>{$cat->name}</option>";
			
		}
	?>
		<option value=''>None</option>
	</select><br>
	<br>
	<input type="submit" name="submit" value="Add Category" />

</form>
</div>

</body>
</html>