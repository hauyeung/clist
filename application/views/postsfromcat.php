<?php include('header.php') ?>
<title>Posts</title>
</head>
<body>

<div id="container">
<table class="table">
	<tr>
		<th>Title</th>
		<th>Text</th>
		<th>Time</th>
		<th>View</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
<?php
	foreach ($posts->result() as $post){
		$deleteurl = base_url("index.php/posts/delete/{$post->postsid}");
		$editurl = base_url("index.php/posts/edit/{$post->postsid}");
		$viewurl = base_url("index.php/posts/view/{$post->postsid}");
		echo '<tr>';
		echo "<td>{$post->title}</td>";
		echo "<td>{$post->text}</td>";
		echo "<td>{$post->createdate}</td>";
		echo "<td><a href='$viewurl'>View</a></td>";
		echo "<td>";
		if (isset($_COOKIE['usersid']) && $_COOKIE['usersid'] == $post->usersid){			
			echo "<a href='{$editurl}'>Edit Post</a>";
		}
		echo "</td>";
		echo "<td>";
		if (isset($_COOKIE['usersid']) && $_COOKIE['usersid'] == $post->usersid){			
			echo "<a href='{$deleteurl}'>Delete</a>";
		}
			echo "</td>";
			
			
		echo '</tr>';
	}
?>
</table>
</div>

</body>
</html>