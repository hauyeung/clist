<?php
class catsmodel extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getcats(){
		return $this->db->get('categories');
	}
	
	public function addparentcat($name){
		$data = array('name' => $name);
		$this->db->insert('categories', $data);
		return $this->db->affected_rows();
	}
	
	public function addchildcat($parentid, $name){		
		$data = array('name' => $name, 'parentcategoriesid' => $parentid);
		$this->db->insert('categories', $data);
		return $this->db->affected_rows();
	}
	
	public function editcat($id, $name){
		return $this->db->simple_query("update categories set name='$name' where categoriesid=$id");
	}
	
	public function deletecat($id){
		return $this->db->query("delete from categories where categoriesid=$id");
	}
	
	public function getchildcats(){
		return $this->db->query('select c2.categoriesid, c1.name as category, c2.name as child, c2.parentcategoriesid from categories as c1 inner join categories as c2 on c1.categoriesid = c2.parentcategoriesid');
	}
	
	public function getparentcats(){
		return $this->db->query('select * from categories where parentcategoriesid is null');
	}
	
	public function getparentcat($id){
		return $this->db->query("select * from categories where parentcategoriesid=$id");
	}
	
	public function getcatbyid($id){
		return $this->db->query("select * from categories where categoriesid=$id");
	}
	
	public function getchildcatsbyid($id){
		return $this->db->query("select c2.categoriesid, c1.name as category, c2.name as child, c2.parentcategoriesid from categories as c1 inner join categories as c2 on c1.categoriesid = c2.parentcategoriesid where c2.categoriesid=$id");
	}
	
}