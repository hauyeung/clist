<?php
class authmodel extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function adduser($user){
		$data = array('name' => $user->name,
		'password' => $user->password, 'usertype' => 'user');
		return $this->db->insert('users', $data);
	}
	
	public function edituser($id, $password){
		return $this->db->query("update users set password='$password' where usersid=$id");
	}
	
	public function removeuser($id){
		return $this->db->query("delete from users where usersid=$id");
	}
	
	public function authenticate($name, $password){
		return $this->db->get_where('users', array('name'=> $name, 'password' => $password));
	}
	
	public function getuser($id){
		return $this->db->get_where('users', array('usersid'=> $id));
	}
}