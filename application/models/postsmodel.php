<?php
class postsmodel extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getposts(){
		return $this->db->get('posts');
	}
	
	public function addpost($post){
		$data = array('title' => $post->title,
		'text' => $post->text,
		'categoriesid' => $post->categoriesid,
		'createdate' => date("Y-m-d H:i:s"),
		'usersid' => $post->usersid);
		return $this->db->insert('posts', $data);
	}
	
	public function editpost($id, $title, $text){
		$updatetitle = $this->db->simple_query("update posts set title='$title' where postsid=$id");
		$updatetext = $this->db->simple_query("update posts set text='$text' where postsid=$id");
		return $updatetitle && $updatetext;
	}
	
	public function deletepost($id){
		return $this->db->delete('posts', array('postsid'=>$id));
	}
	
	public function getpostsfromcat($catid){
		return $this->db->query("select * from posts where categoriesid=$catid order by createdate desc");
	}
	
	public function getpostbyid($postsid){
		return $this->db->get_where('posts', array('postsid' => $postsid));
	}
}