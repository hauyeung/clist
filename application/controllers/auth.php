<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->view('login');
	}
	
	public function login(){
		$this->load->model('authmodel');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$name = $this->input->post('username');
		$password = $this->input->post('password');		
		$query = $this->authmodel->authenticate($name, $password);
		if (count($query->result()) > 0){
			setcookie('loggedin', true, time()+60*60*24*120, '/');
			$result = $query->result();
			setcookie('usersid', $result[0]->usersid, time()+60*60*24*120, '/');
			if ($result[0]->usertype == 'admin'){
				setcookie('isadmin', true, time()+60*60*24*120, '/');
			}
			
			redirect(base_url());
		}
		else{
			redirect('/auth/index');
		}
	}
	
	public function register(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->view('register');
	}
		
	public function add(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_message('password', 'Please enter password.');
		$this->form_validation->set_rules('confirmpassword', 'confirmpassword', 'required');
		$this->form_validation->set_message('confirmpassword', 'Please enter password.');
		$this->load->model('authmodel');
		
		$u = new user();
		$u->name = $this->input->post('username');
		$u->password = $this->input->post('password');
		$confirmpassword = $this->input->post('confirmpassword');		
		if ($this->form_validation->run()){
			if ($u->password == $confirmpassword && $confirmpassword != ''){
				$query = $this->authmodel->adduser($u);
				$this->load->view('header');
				$this->load->view('success');
				$this->load->view('footer');
			}
		}
		else{
			$this->load->view('register');
		}
			
		
	}
	
	public function editform(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->view('edituser');		
	}
	
	public function edit(){
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_message('password', 'Please enter password.');
		$this->form_validation->set_rules('confirmpassword', 'confirmpassword', 'required');
		$this->form_validation->set_message('confirmpassword', 'Please enter password.');
		$this->load->model('authmodel');		
		$password = $this->input->post('password');
		$confirmpassword = $this->input->post('confirmpassword');
		if ($this->form_validation->run()){
			if ($password == $confirmpassword && isset($_COOKIE['usersid'])){
				$query = $this->authmodel->edituser($_COOKIE['usersid'], $password);												
				if ($query !== false){
					$this->load->view('header');
					$this->load->view('success');
					$this->load->view('footer');
				}
			}			
		}
		else{
			$this->load->view('edituser');	
		}
			
	}
	
	public function logout(){
		setcookie('loggedin', null, -1, '/');
		setcookie('usersid', null, -1, '/');
		setcookie('isadmin', null, -1, '/');
		redirect('/');
		
	}
		
}

class user{
	var $name;
	var $password;	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */