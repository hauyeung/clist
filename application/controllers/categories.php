<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class categories extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$this->load->model('catsmodel');
		$data['cats'] = $this->catsmodel->getcats();
		$this->load->view('cathome', $data); 
	}
	
	public function addform()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('catsmodel');		
		$data['parentcats']  = $this->catsmodel->getparentcats();
		$this->load->view('addcat', $data);
	}	
	
	public function add(){
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('catsmodel');		
		$this->load->model('authmodel');		
		$user = $this->authmodel->getuser($_COOKIE['usersid']);
		$result = $user->result();
		if(!isset($_COOKIE['usersid']) || $result[0] ->usertype != 'admin'){
			redirect('/');
		}
		
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_message('name', 'Please enter a name.');
		$data['parentcats']  = $this->catsmodel->getparentcats();
		
		
		if ($this->form_validation->run())
		{		
			if ($this->input->post('parentcategoriesid') == ''){
				$name = $this->input->post('name');
				$rows = $this->catsmodel->addparentcat($name);					
			}
			else{
				$name = $this->input->post('name');
				$parentid = $this->input->post('name');
				$rows = $this->catsmodel->addchildcat($parentid, $name);								
			}
			$this->load->view('header');
			$this->load->view('success');
			$this->load->view('footer');
		}		
	}
	
	public function editform($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('catsmodel');
		$this->load->model('authmodel');
		$user = $this->authmodel->getuser($_COOKIE['usersid']);
		$result = $user->result();
		if(!isset($_COOKIE['usersid']) || $result[0]->usertype != 'admin'){
			redirect('/');
		}
		$data['parentcat'] = $this->catsmodel->getchildcatsbyid($id);	
		$cat = $this->catsmodel->getcatbyid($id)->result();
		$data['name'] = $cat[0]->name;
		$this->load->view('editcat', $data);
	}
	
	public function edit(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('catsmodel');		
		$this->load->model('authmodel');		
		$user = $this->authmodel->getuser($_COOKIE['usersid']);
		$result = $user->result();
		if(!isset($_COOKIE['usersid']) || $result[0]->usertype != 'admin'){
			redirect('/');
		}
		$id = $this->input->post('catid');
		$data['parentcat'] = $this->catsmodel->getchildcatsbyid($id);	
		$n = $this->catsmodel->getcatbyid($id)->result();	
		$data['name'] = $n[0]->name;
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_message('name', 'Please enter a name.');						
		if ($this->form_validation->run())
		{		

			$name = $this->input->post('name');
			
			$rows = $this->catsmodel->editcat($id, $name);		
			$this->load->view('header');
			$this->load->view('success');
			$this->load->view('footer');
		}	
	}
	
	public function delete($catid){
		$result = $user->results();
		if(!isset($_COOKIE['usersid']) || $result[0]->usertype != 'admin'){
			redirect('/');
		}
		else{
			$this->load->model('catsmodel');	
			$data['posts'] = $this->catsmodel->deletecat($catid);
			redirect('/');
		}
			
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */