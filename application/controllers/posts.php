<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class posts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('postsmodel');
		$data['posts'] = $this->postsmodel->getposts();
		$this->load->view('postshome', $data);
	}
	
	public function addform(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('catsmodel');
		$data['cats'] = $this->catsmodel->getchildcats();
		$this->load->view('addpost', $data);
	}
	
	public function add(){
		if (!isset($_COOKIE['usersid'])){
			redirect('auth/login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');		
		$this->load->model('catsmodel');
		$this->load->model('postsmodel');
		$data['cats'] = $this->catsmodel->getchildcats();
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_message('title', 'Please enter a title.');
		$this->form_validation->set_rules('text', 'text', 'required');
		$this->form_validation->set_message('title', 'Please enter text.');
		
		if ($this->form_validation->run())
		{	
			$p = new post();
			$p->text = $this->input->post('title');
			$p->title = $this->input->post('text');
			$p->usersid = $this->input->cookie('usersid');
			$p->categoriesid = $this->input->post('categoriesid');					
			$query = $this->postsmodel->addpost($p);			
			$this->load->view('header');
			$this->load->view('success');
			$this->load->view('footer');
		}		
		else{
			$this->load->view('addpost', $data);
		}
					
	}
	
	public function editform(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->view('editpost');
	}
	
	public function edit(){
		if (!isset($_COOKIE['usersid'])){
			redirect('auth/login');
		}
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('postsmodel');
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_message('title', 'Please enter a title.');
		$this->form_validation->set_rules('text', 'text', 'required');
		$this->form_validation->set_message('title', 'Please enter text.');
		
		if ($this->form_validation->run())
		{	
			$id = $this->input->post('id');	
			$text = $this->input->post('title');
			$title = $this->input->post('text');			
			$query  = $this->postsmodel->editpost($id, $title, $text);
			if ($query){
				$this->load->view('header');
				$this->load->view('success');
				$this->load->view('footer');
			}
			
		}	
		else{
			$this->load->view('editpost');
		}
	}
	
	public function delete($id){	
		if (!isset($_COOKIE['usersid'])){
			redirect('auth/login');
		}	
		$this->load->model('postsmodel');		
		$query = $this->postsmodel->deletepost($id);
		if ($query){			
			redirect('/');
		}
		
	}
	
	public function getpostsfromcat($catid){		
		$this->load->model('postsmodel');	
		$data['posts'] = $this->postsmodel->getpostsfromcat($catid);
		$this->load->view('postsfromcat', $data);
	}
	
	public function view($postsid){
		$this->load->model('postsmodel');
		$data['post'] = $this->postsmodel->getpostbyid($postsid);
		$this->load->view('postbyid', $data);
	}
	
}

class post{
	var $text;
	var $title;
	var $usersid;
	var $categoriesid;
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */