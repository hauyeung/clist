drop database if exists craigslist;
create database craigslist;
use craigslist;

create table categories (
 categoriesid int not null auto_increment primary key,
 name varchar(200),
 parentcategoriesid int
);

create table posts(
postsid int not null auto_increment primary key,
title varchar(200),
text text,
categoriesid int not null,
createdate datetime,
usersid int not null
);

create table users(
usersid int not null auto_increment primary key,
name varchar(200),
password varchar(200)
);

alter table posts
add constraint fkcategories foreign key (categoriesid) references categories(categoriesid);

alter table posts
add constraint fkusers foreign key (usersid) references users(usersid);

alter table users add unique (name);

alter table users add usertype varchar(200);

insert into categories values ('', 'buy',null);
insert into categories values ('', 'sell',null);
insert into categories values ('', 'groceries',0);
insert into categories values ('', 'groceries',1);

insert into users values ('','user','password', 'user');
insert into users values ('','user2','password', 'admin');

insert into posts values ('', 'Chicken', 'chicken',1,now(), 1);
insert into posts values ('', 'Chicken', 'chicken',1,now(), 2);
insert into posts values ('', 'Chicken', 'chicken',2,now(), 1);
insert into posts values ('', 'Chicken', 'chicken',2,now(), 2);